# runwayML

![](https://gitlab.com/frankiezafe/runwayml/-/raw/master/screenshots/densedepth-godot-shader.png)

notes and projects related to http://runwayml.com

## installing linux mint 19

- go to [linux mint 19.2 download page](https://www.linuxmint.com/edition.php?id=267);
- download Linux Mint 19.2 "Tina" - Cinnamon (64-bit) iso;
- create a bootable usb drive with [rufus](https://rufus.ie/).

See [Create a bootable USB stick on Windows](https://ubuntu.com/tutorials/tutorial-create-a-usb-stick-on-windows#1-overview) for more details.

Linux Mint & RunwayML system requirements:

- 1GB RAM (2GB recommended for a comfortable usage);
- 15GB of disk space (20GB recommended);
- Cuda compatible GPU (NVidia GeForce 960 or higher);
- 50GB of disk space for RunwayML and datasets.

About the 50GB for RunwayML: running models locally requires a lot of disk space. Indeed, they weight between 700MB and 8GB (DenseDepth) each.

In order to use the local GPU, NVidia proprietary drivers have to be installed.

Go to *System Settings > Drivers Manager* and install the recommended nvidia-driver and reboot the computer. If you encounter issue with this method, see [Install latest NVIDIA drivers for Linux Mint 19/Ubuntu 18.04](https://blog.softhints.com/install-latest-nvidia-drivers-ubuntu-mint/)

## installing RunwayML on linux mint 19

- go to [RunwayML for linux](https://runwayml.com/download) and download the executable;
- verify execution rights with `ls -la` in the folder containing the executable, Runway image should have `-rwxr-xr-x` rights;
- to add execution rights, use `sudo chmod +x ./Runway\ 0.10.26.AppImage`;
- launch the executable in terminal using: `sudo ./Runway\ 0.10.26.AppImage`;
- create an account or login.

In order to install models locally, runway must have the rights to use docker. A solution to use docker without sudo is described in this [article](https://support.runwayml.com/en/articles/3359183-running-docker-without-sudo).

## running models locally on linux mint 19

### installing docker

for Linux Mint 19 use "bionic" apt-repository!

```
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   bionic \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo docker run hello-world
```

[src0](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

to enable GPU support:

```
sudo docker volume ls -q -f driver=nvidia-docker | xargs -r -I{} -n1 docker ps -q -a -f volume={} | xargs -r docker rm -f
sudo apt-get purge nvidia-docker
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/ubuntu18.04/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update
sudo apt-get install nvidia-docker2
docker run --runtime=nvidia --rm nvidia/cuda nvidia-smi
```
last command will download required cuda dependencies

[src1](https://support.runwayml.com/en/articles/3140813-local-gpu) [src2](https://github.com/NVIDIA/nvidia-docker/wiki/Installation-(version-2.0))

runwayML must be run as sudo! so:

```
sudo ./Runway\ 0.10.25.AppImage
```
